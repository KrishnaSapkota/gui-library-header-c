#include<iostream.h>
#include<string.h>
#include<stdlib.h>
#include<conio.h>
#include<stdio.h>
#include<dos.h>
#include<graphics.h>
#include<fstream.h>
#define ClicKed 1
#define Not_ClicKed 0
#define SHOW 1
#define HIDE 2
const int MENU=3;
const int CHOICE=4;
enum boolean{false,true};
char *dfilename="image.ftr";
class Mouse
{
	private:
				union REGS input,output;
	public:
				Mouse(){};
				int initialise_mouse();
				pointer(int ON);
				restrict_pointer(int X1,int Y1,int X2,int Y2);
				change_pointer(int X1,int Y1);
				int get_pointer_position(int *Button,int *X,int *Y);
};
class Pixels
{
	public:
			int pixelcolor,x,y;
			int width,height;
			Pixels(){};
			void putimage();
			void putimage(int ,int);
			void write(int x1,int y1,int p);
			void writewh(int width,int height);
			void getSize(int *,int *);
};
class Image
{
	private:
			int xp1,yp1,xp2,yp2;
			fstream imgfile;
			char filename[12];
			Mouse m;
	public:
			Pixels img;
			Image()
			{
				strcpy(filename,dfilename);
				imgfile.open(filename,ios::in|ios::out|ios::binary);
			}
			Image(char fn[12])
			{
				strcpy(filename,fn);
				imgfile.open(filename,ios::in|ios::out|ios::binary);
			}
			void getImage(int x1,int y1,int x2,int y2);
			void putImage();
			void getSize(int &,int &);
			void putImage(int,int);
			void clearFile();
};
class Component
{
  protected:
				boolean activated;
				boolean border;
   public:

			   Component(){activated=false;}
//			   virtual void show()=0;
			   virtual void activateComponent(boolean activate)=0;
};
class Window
{
		 protected:
					 int Window_Color,bar_color,textcolor;
					 char Window_Name[60];
					 struct date d;
					 boolean write_date;
					 boolean showbar;
					 boolean originsetted;
					 void WRITE_DATE(int,int);
		 public:
					 int barheight;
					 int X1,Y1,X2,Y2;
					 Window();
					 Window(char title[]);
					 Window(int,int,int,int,int,char [],int barcol=BLUE,boolean date=true,boolean showbar=true);
					 void setTitle(char title[]);
					 void setOrigin(int,int);
					 void setSize(int,int);
					 void setBarColor(int);
					 void setColor(int);
					 void setTitleColor(int);
					 void setDate(boolean);
					 void setBar(boolean);
					 void show();
					 void activateComponent(boolean activate);
					 void sLine(int,int,int,int);
};
class Button:public Mouse,public Component
{
		protected:
				   int button,x,y;
				   int Text_Color,Button_Color;
				   char Button_Name[30];
				   void Unpushed_Button(int,int,int,int,int);
				   void Pushed_Button(int,int,int,int,int);
				   boolean boarder;
		public:
				   int X1,Y1,X2,Y2;
				   Button(){};
				   Button(int x1,int y1,char blabel[]);
				   Button(int x1,int y1,int x2,int y2,int col,int t_col,char But_Name[]);
				   Button(Button &,int,int,char []);
				   void initialiseButton(int x1,int y1,int x2,int y2,int col,int t_col,char But_Name[]);
				   void initialiseButton(int x1,int y1,char blabel[]);
				   void setText(char blabel[]);
				   void setColor(int color);
				   void setLabelColor(int color);
				   void setHeight(int height);
				   void setWidth(int width);
				   void setBoarder(boolean flag);
				   void show();
				   int toggle();
				   void activateComponent(boolean activate);
};
void Button::activateComponent(boolean activate)
{
	activated=activate;
	if(activated)
	{
		pointer(HIDE);
		setcolor(BLACK);
		setlinestyle(1,1,1);
		rectangle(X1-3,Y1-3,X2+3,Y2+3);
		pointer(SHOW);
	}
	else
	{
		pointer(HIDE);
		setcolor(getpixel(X1-3,Y1-3));
		setlinestyle(1,1,1);
		rectangle(X1-3,Y1-3,X2+3,Y2+3);
		pointer(SHOW);
	}
}

class IconButton :public Button
{
		private:
				   char iconfn[12];
				   Image icon;
				   int iconwidth,iconheight;
		public:
				   IconButton(int x1,int y1,int c,char icfn[12]);
				   void show();
				   int toggle();
};
class Menu :public Mouse,public Component
{
	private:
		int nw,nh;
	protected:
			char menuname[20];
			char menuitem[50][30];
			int itemindex;
			int currenty1;
			int X1,Y1,X2,Y2,width,height;
			int menucolor,textcolor,hlcolor,hltextcolor;
			int disableitemcolor;
			int button,mx,my,highlightheight,tx,ty;
			int selectedindex,separator[50],disable[50],l;
			char *selecteditem;
			int highlightedposition;
			int ewidth,type;
			int itmindx;
			boolean boarder;
			Image *menuimg;
			void findSize();
			void drawMenu();
			void drawSeparator();
			void setExtraWidth(int ewidth);
			void HighlightItem();
			void deHighlightItem();
			void dispose(int);
			Menu(){};
	public:
			Menu(int x,int y,char []);
			void add(char []);
			void addSeparator();
			void add(int index,char item[]);
			void remove(int index);
			void remove(char item[]);
			void setEnable(int index,boolean flag);
			void setEnable(char item[],boolean flag);
			boolean toggle();
			void show();
			void handleMenu();
			void handleMenu(const int x,const int y);
			int getSelectedIndex();
			char *getSelectedItem();
			void setColor(int color);
			void setHighlightColor(int color);
			void setTextColor(int color);
			void setHighlightTextColor(int color);
			void setDisableTextColor(int color);
			void setBoarder(boolean flag);
			void activateComponent(boolean activate);
};
void Menu::activateComponent(boolean activate)
{
 activated=activate;
}
class Choice :public Menu
{
  private:
		Button *choicebutton;
		int prevselected;
		int cx,cy;
  public:
		Choice(int x,int y);
		void show();
		boolean toggle();
		void setSelectedIndex(int index);
		void setSelectedItem(char item[]);
		void activateComponent(boolean activate);
		void handle();
};
void Choice::activateComponent(boolean activate)
{
	activated=activate;
}
class Frame :public Window
{
		 protected:
					 char winflnam[10];
					 Button *winclosebutton;
					 Mouse m;
		 public:
					 Frame(){};
					 Frame(char title[],char wfn[]);
					 void setSize(int,int);
					 void show();
					 boolean windowClosed();
					 void closeFrame();
					 ~Frame(){};
};
class TextField:public Component
{
	protected:
					int fieldcolor,textcolor,cursorcolor;
					int width;
					int b,x,y;
					int HEIGHT,WNO;
					int cursorheight;
					int c,w;
					char ch;
					char *str,*len;
					Mouse Mousehandler;
					void showField();
					void printText();
	 public:
					int xPos,yPos;
					enum key{notpressed,pressed};
					key escapekey;
					boolean textpresent;
					boolean editable;
					TextField();
					TextField(int x,int y,int wordno);
					void initialiseTextField(int,int,int);
					void show();
					void setEditable(boolean);
					void setText(char []);
					void setTextColor(int);
					void setFieldColor(int);
					void setCursorColor(int);
					void setBorder(boolean);
					boolean isEditable();
					boolean toggle();
					char *handle();
					char *getText();
					int getTextColor();
					int getCursorColor();
					int getFieldColor();
					void activateComponent(boolean activate);
};
TextField::TextField(int x,int y,int wordno)
{

				WNO=wordno;
				str=new char[WNO+1];
				HEIGHT=17;
				fieldcolor=15;
				textcolor=0;
				cursorcolor=0;
				cursorheight=17;
				xPos=x;
				yPos=y;
				len=new char[WNO+1];
				for(int i=0;i<=WNO;i++)
				len[i]=' ';
				len[--i]='\0';
				width=textwidth(len)+4;
				delete len;
				escapekey=notpressed;
				textpresent=false;
				editable=true;
				border=true;
				c=0;
				w=1;
}
class TextArea:public TextField
{
	int linediff;
	int row,column;
	int cursorx,cursory,onelinech;
	int printx,printy;
	int max,maxnoofline;
	char *aline;
	public:
		TextArea():TextField()
		{
		     HEIGHT=13;
		     cursorx=xPos;
		     cursory=yPos;
		     onelinech=0;
		     row=5;
		     column=10;
		     HEIGHT=HEIGHT*5+2;
		     linediff=15;
		     max=0;
		     maxnoofline=0;
		}
		TextArea(int x,int y,int column,int row):TextField(x,y,column)
		{
		     HEIGHT=13;
		     this->row=row;
		     this->column=column;
		     onelinech=0;
		     HEIGHT=HEIGHT*row+2;
		     linediff=13;
		     cursorx=xPos;
		     cursory=yPos;
		     max=0;
		     maxnoofline=0;
		}
		void handle();
		void printText();
};
void TextArea::printText()
{
     int read=0,start=0,i=0;
     printx=xPos;
     printy=yPos;
     aline=new char[column+1];
     setcolor(textcolor);
     while(read<=max)
     {
	aline[start]=str[i];
	i++;
	start++;
	read++;
	if(start<=column)
	{
		aline[start]='\0';
		outtextxy(printx+4,printy+4,aline);
	}
	if(start>column)
	{

	   aline[0]=str[max];
	   printy+=linediff;
	   aline[1]='\0';
	   outtextxy(printx+4,printy+4,aline);
	   start=0;
	   i=i-1;
	   read=read-1;
	}

     }
}
void TextArea::handle()
{

	int t,index;
	max=0;

	setlinestyle(0,0,0);
	showField();
	settextstyle(DEFAULT_FONT,HORIZ_DIR,1);
	Mousehandler.pointer(HIDE);
	max=onelinech;
	if(textpresent==true)
	{
		index=(cursory-yPos)/linediff;
		c=(w+index*column)-1;
		printText();
	}
	else
	{
		w=1;
		c=0;
		strcpy(str,"");
		showField();
	}

	 int key=0;

	 do
	  {
		while(!kbhit())
		{
			setcolor(cursorcolor);
			line(cursorx+w*8-5,cursory+3,cursorx+w*8-5,cursory+cursorheight-3);
			delay(100);
			setcolor(fieldcolor);
			line(cursorx+w*8-5,cursory+3,cursorx+w*8-5,cursory+cursorheight-3);
			delay(120);
		}
		ch=getch();
		setcolor(fieldcolor);
		if(ch==8&&c>0)
		{
		     if(w==1&&maxnoofline>0)
		     {
			cursory-=linediff;
			w=column+1;
			max--;
			index=(cursory-yPos)/linediff;
			c=(w+index*column)-1;
		     }
		     else
		     max--;
		     w--;
		     c--;
		     onelinech--;
		     for(int i=c;i<max;i++)
		     str[i]=str[i+1];
		     str[max]='\0';
		     showField();
		}
		if(c>max)
		max=c;
		int tabkey=(int)ch;
		if(tabkey==9)
		{
		   if(c!=0)
		   textpresent=true;
		}
		if(ch==0&&c>=0)
		{
		    key=getch();
		    if(key==75&&c!=0)//left arrow pressed
		     {
			if(maxnoofline>0&&w==1)
			{
				w=column+1;
				cursory-=linediff;
			}
			else
			{
				w--;
				c--;
			}
			str[max]='\0';

		      }
		     if(key==77&&w<=max)//right arrow pressed
			{
			  if(maxnoofline>0&&w==column+1)
			  {
			     w=1;
			     cursory+=linediff;
			  }
			  index=(cursory-yPos)/linediff;
			  c=(w+index*column)-1;
			  if(max>c)
			  {
				  w++;
				  c++;
			  }
			  if(c>max)
			  max=c;
			  str[max]='\0';
			}
		       if(key==83&&c<=max)//delete key
			{
				for(int i=c;i<=max;i++)
				str[i]=str[i+1];//delete the character from the arrayindex,c
				max--;
				onelinech--;
				showField();
			}
			if(key==71)//home key
			{
				w=1;
				index=(cursory-yPos)/linediff;
				c=(w+index*column)-1;
			}
			if(key==72&&cursory>yPos)
			{
				cursory-=linediff;
				index=(cursory-yPos)/linediff;
				c=(w+index*column)-1;
			}
			index=(cursory-yPos)/linediff;
			if(key==80&&index<row-1&&onelinech>column)
			{
				cursory+=linediff;
				index=(cursory-yPos)/linediff;
				c=(w+index*column)-1;
			 }





		      /*	if(key==79)//end key
			{

				index=(cursory-yPos)/linediff;
			       //	int y=column*index+
				c=max;
				w=column+1;
			}*/
		  }
		  if(c<0)
		  c=0;
		  if(w==0)
		  w=1;
		  if(ch!='\r'&&ch!=8&&ch!=27&&key!=75&&key!=77&&key!=83&&key==0&&c<column*row)
		    {
			 onelinech++;
			 if(c<=max)
			 {
			   for(int i=max;i>=c;i--)
			   str[i+1]=str[i];
			   str[c++]=ch;
			   max++;
			 }
			else
			str[c++]=ch;
			if(c<=max)
			showField();
			if(c>max)
			max=c;
			if(w>=column+1)
			{
			    cursory+=linediff;
			    cursorx=xPos;
			    w=2;
			    maxnoofline++;
			}
			else
			w++;
			str[max]='\0';
		    }
		    printText();

		    key=0;
	}while(ch!='\r');
  Mousehandler.pointer(SHOW);
  if(c!=0)
  textpresent=true;
}


void TextField::activateComponent(boolean activate)
{
   activated=activate;
   if(activated&&editable)
	{
		Mousehandler.pointer(HIDE);
		setcolor(BLACK);
		setlinestyle(1,1,1);
		rectangle(xPos-4,yPos-4,xPos+width+4,yPos+HEIGHT+4);
		Mousehandler.pointer(SHOW);

		this->handle();
	}
	else
	{
		Mousehandler.pointer(HIDE);
		setcolor(getpixel(xPos-5,yPos));
		setlinestyle(1,1,1);
		rectangle(xPos-4,yPos-4,xPos+width+4,yPos+HEIGHT+4);
		Mousehandler.pointer(SHOW);
	 }

}
class PasswordField:public TextField
{
		private:
				char *password;
				char passchar;
				void printPassword();
		public:
				PasswordField(){};
				PasswordField(int x,int y,int WNO):TextField(x,y,WNO)
				{
					password=new char[WNO+1];
					passchar='*';
				}
				void initialisePasswordField(int x,int y,int WNO);
				void setCharacter(char ch);
				void setText(char []);
				void setEditable(boolean);
				void show();
				void activateComponent(boolean activate);
				char *handle();
};
void PasswordField::activateComponent(boolean activate)
{
   activated=activate;
   if(activated)
	{
		setcolor(BLACK);
		setlinestyle(1,1,1);
		rectangle(xPos-3,yPos-3,xPos+width+3,yPos+HEIGHT+3);
		this->handle();
	}
	else
	{
		setcolor(getpixel(xPos-5,yPos));
		setlinestyle(1,1,1);
		rectangle(xPos-4,yPos-4,xPos+width+4,yPos+HEIGHT+4);
	 }

}
class MessageBox
{
		private:
					Mouse m;
					Image *bximg;
					int fillcolor,barcolor,textcolor;
					char message_title[30],message[600];
					int messglength,buttxpos;
					int X1,Y1,X2,Y2;
					void showMessageBox();
		public:
					MessageBox(){
					X1=getmaxx()/2-100;
					Y1=getmaxy()/2-70;
					X2=getmaxx()/2+100;
					Y2=getmaxy()/2+70;
					fillcolor=7;
					barcolor=1;
					textcolor=0;
					}
					void show();
					void showMessageBox(char *,char *);
					void setColor(int color);
					void setTextColor(int color);
					void setBarColor(int color);
};
void MessageBox::show()
{
	this->showMessageBox();
}
class KeyboardListener
{
	protected:
			   Component* listenerlist[30];
			   int listenercount;

	public:
			 int uiobject,key;
			 KeyboardListener(){listenercount=0;uiobject=0;}
			 void add(TextField &txtf);
			 void add(PasswordField &pas);
			 void add(Menu &mn);
			 void add(Button &b);
			 void add(Choice &ch);
			 void add(IconButton &ib);
			 void enableKeyboardAccess();
};
void KeyboardListener::enableKeyboardAccess()
{

 if(kbhit())
 {
	key=getch();
	if(key==9)
	{
		if(uiobject<listenercount)
			{
				if(uiobject>0)
				listenerlist[uiobject-1]->activateComponent(false);
				listenerlist[uiobject]->activateComponent(true);
				uiobject++;
			}
		  else
			{
				listenerlist[uiobject-1]->activateComponent(false);
				uiobject=0;
				listenerlist[uiobject]->activateComponent(true);
			}
	 }
  }
	   return;

 }
void KeyboardListener::add(TextField &txtf)
{
   listenerlist[listenercount]=&txtf;
   listenercount++;
}
void KeyboardListener::add(PasswordField &pass)
{
   listenerlist[listenercount]=&pass;
   listenercount++;
}
void KeyboardListener::add(Menu &mn)
{
   listenerlist[listenercount]=&mn;
   listenercount++;
}
void KeyboardListener::add(Button &bt)
{
   listenerlist[listenercount]=&bt;
   listenercount++;
}
void KeyboardListener::add(Choice &ch)
{
   listenerlist[listenercount]=&ch;
   listenercount++;
}
void KeyboardListener::add(IconButton &ib)
{
   listenerlist[listenercount]=&ib;
   listenercount++;
}
void Pixels::write(int x1,int y1,int p)
{x=x1;y=y1;pixelcolor=p;}
void Pixels::writewh(int w,int h)
{width=w;height=h;}
void Pixels::getSize(int *w,int *h)
{*w=width;*h=height;}
void Pixels::putimage()
{putpixel(x,y,pixelcolor);}
void Pixels::putimage(int x,int y)
{putpixel(x,y,pixelcolor);}
void Image::getImage(int x1,int y1,int x2,int y2)
{
	m.pointer(HIDE);
	xp1=x1;
	xp2=x2;
	yp1=y1;
	yp2=y2;
	int height=yp2-yp1;
	int width=xp2-xp1;
	int x,y,pixelcolor,count=0;
	imgfile.seekp(0);
	imgfile.clear();
	for(int i=y1;i<=y2;i++)
		{
			for(int j=x1;j<=x2;j++)
				{
					x=j;
					y=i;
					pixelcolor=getpixel(j,i);
					if(count==0)
						{
							img.writewh(width,height);
							imgfile.clear();
							imgfile.write((char*)&img,sizeof img);
							imgfile.seekp(0,ios::end);
						}
					img.write(x,y,pixelcolor);
					imgfile.clear();
					imgfile.write((char*)&img,sizeof img);
					imgfile.seekp(0,ios::end);
					count=1;
				  }

		  }
		  m.pointer(SHOW);
 }
void Image::putImage()
{
   m.pointer(HIDE);
   imgfile.seekp(sizeof(img));
   while(imgfile.read((char*)&img,sizeof img))
	{
	   img.putimage();
	}
	imgfile.close();
	m.pointer(SHOW);
}
void Image::getSize(int &x,int &y)
{
	imgfile.seekp(0,ios::beg);
	imgfile.read((char*)&img,sizeof img);
	img.getSize(&x,&y);
}
void Image::putImage(int x,int y)
{
	m.pointer(HIDE);
	int height;
	int width;
	imgfile.seekp(0,ios::beg);
	imgfile.read((char*)&img,sizeof img);
	img.getSize(&width,&height);
	for(int i=0;i<=height;i++)
	{
		for(int j=0;j<=width;j++)
		 {
			imgfile.read((char*)&img,sizeof img);
			img.putimage(x+j,y+i);
		 }
	 }
	 m.pointer(SHOW);
	 imgfile.close();
}
void Image::clearFile()
{
	 imgfile.open(filename,ios::in|ios::out|ios::binary|ios::trunc);
	 imgfile.close();
}

Frame::Frame(char title[60],char wfn[10])
{
	strcpy(Window_Name,title);
	strcpy(winflnam,wfn);
	Window_Color=7;
	bar_color=1;
	textcolor=YELLOW;
	barheight=20;
	write_date=false;
	showbar=true;
	originsetted=false;
	X1=getmaxx()/2-150;
	X2=getmaxx()/2+150;
	Y1=getmaxy()/2-120;
	Y2=getmaxy()/2+120;
	winclosebutton=new Button(X1-20,Y1+2,"X");
	winclosebutton->setWidth(16);
	winclosebutton->setHeight(15);
}
Window::Window()
{
	Window_Color=7;
	bar_color=1;
	textcolor=YELLOW;
	barheight=20;
	write_date=false;
	showbar=true;
	originsetted=false;
	X1=getmaxx()/2-150;
	X2=getmaxx()/2+150;
	Y1=getmaxy()/2-120;
	Y2=getmaxy()/2+120;
}
Window::Window(char title[60])
{
	strcpy(Window_Name,title);
	Window_Color=7;
	bar_color=1;
	textcolor=YELLOW;
	barheight=20;
	write_date=false;
	showbar=true;
	originsetted=false;
	X1=getmaxx()/2-150;
	X2=getmaxx()/2+150;
	Y1=getmaxy()/2-120;
	Y2=getmaxy()/2+120;
}
Window::Window(int x1,int y1,int x2,int y2,int color,char Win_name[],int barcolor,boolean date,boolean bar)
{
	strcpy(Window_Name,Win_name);
	Window_Color=color;
	bar_color=barcolor;
	textcolor=YELLOW;
	barheight=20;
	write_date=date;
	showbar=bar;
	X1=x1;
	Y1=y1;
	X2=x2;
	Y2=y2;
}
Button::Button(int x1,int y1,int x2,int y2,int col,int t_col,char But_Name[])
{
	strcpy(Button_Name,But_Name);
	Button_Color=col;
	Text_Color=t_col;
	X1=x1;
	Y1=y1;
	X2=x2;
	Y2=y2;
	boarder=true;
}
Button::Button(int x1,int y1,char But_Name[])
{
	strcpy(Button_Name,But_Name);
	Button_Color=7;
	Text_Color=0;
	X1=x1;
	Y1=y1;
	X2=strlen(But_Name)*10+X1;
	Y2=y1+20;
	boarder=true;
}

Button::Button(Button &button,int col,int tcol,char nam[])
{
	X1=button.X1;
	Y1=button.Y1;
	X2=button.X2;
	Y2=button.Y2;
	Text_Color=tcol;
	Button_Color=col;
	strcpy(Button_Name,nam);
	boarder=true;
}
void Button::initialiseButton(int x1,int y1,char But_Name[])
{
	strcpy(Button_Name,But_Name);
	Button_Color=7;
	Text_Color=0;
	X1=x1;
	Y1=y1;
	X2=strlen(But_Name)*10+X1;
	Y2=Y1+20;
	boarder=true;
}

void Button::initialiseButton(int x1,int y1,int x2,int y2,int col,int t_col,char But_Name[])
{
	strcpy(Button_Name,But_Name);
	Button_Color=col;
	Text_Color=t_col;
	X1=x1;
	Y1=y1;
	X2=x2;
	Y2=y2;
	boarder=true;
}
void Button::setText(char But_Name[])
{pointer(HIDE);strcpy(Button_Name,But_Name);show();pointer(SHOW);}
void Button::setColor(int color)
{Button_Color=color;}
void Button::setLabelColor(int color)
{Button_Color=color;}
void Button::setHeight(int height)
{Y2=Y1+height;}
void Button::setWidth(int width)
{X2=X1+width;}
void Button::setBoarder(boolean flag)
{boarder=flag;}
int Button::toggle()
{
	int t1,t2,i,k=0,key;
	setlinestyle(0,1,1);
	for(i=0;i<strlen(Button_Name);i++)
	k=k+4;
	t1=(X2-X1)/2+X1-k;
	t2=(Y2-Y1)/2+Y1;
	get_pointer_position(&button,&x,&y);
	if(activated)
		{

			if(kbhit())
				{
						pointer(HIDE);
						key=getch();
						if(key==13)
							{
								Pushed_Button(X1,Y1,X2,Y2,Button_Color);
								setcolor(Text_Color);
								settextstyle(0,0,0);
								outtextxy(t1+1,t2+1,Button_Name);
								delay(15);
								Unpushed_Button(X1,Y1,X2,Y2,Button_Color);
								settextstyle(0,0,0);
								setcolor(Text_Color);
								outtextxy(t1,t2,Button_Name);
								pointer(SHOW);
								return ClicKed;
							 }
					 }
			 }
	if( x>X1 && x<X2 &&y>Y1 && y<Y2 &&button==1)
	{

							  pointer(HIDE);
							  Pushed_Button(X1,Y1,X2,Y2,Button_Color);
							  setcolor(Text_Color);
							  settextstyle(0,0,0);
							  outtextxy(t1+1,t2+1,Button_Name);
							  pointer(SHOW);
							  while((button==1)&&(x>X1 && x<X2) && (y>Y1 && y<Y2) && (button==1))
							  get_pointer_position(&button,&x,&y);
							  pointer(HIDE);
							  Unpushed_Button(X1,Y1,X2,Y2,Button_Color);
							  settextstyle(0,0,0);
							  setcolor(Text_Color);
							  outtextxy(t1,t2,Button_Name);
							  pointer(SHOW);
							  get_pointer_position(&button,&x,&y);
							  if( (x>X1 && x<X2) && (y>Y1 && y<Y2))
							  return ClicKed;
							  else
							  return Not_ClicKed;
		  }

	pointer(SHOW);
	return Not_ClicKed;

}
void Button::Unpushed_Button(int X1,int Y1,int X2,int Y2,int col)
{
	Button_Color=col;
	setlinestyle(0,1,1);
	setfillstyle(1,Button_Color);
	bar(X1,Y1,X2,Y2);
	if(boarder)
	{
		setcolor(WHITE);
		line(X1,Y1,X2,Y1);
		line(X1,Y1,X1,Y2);
		setcolor(BLACK);
		line(X2,Y1,X2,Y2);
		line(X1,Y2,X2,Y2);
	}
}
void Button::Pushed_Button(int X1,int Y1,int X2,int Y2,int col)
{
	Button_Color=col;
	setlinestyle(0,1,1);
	setfillstyle(1,Button_Color);
	bar(X1,Y1,X2,Y2);
	if(boarder)
	{
		setcolor(BLACK);
		line(X1,Y1,X2,Y1);
		line(X1,Y1,X1,Y2);
		setcolor(WHITE);
		line(X2,Y1,X2,Y2);
		line(X1+1,Y2,X2,Y2);
	}
}
void Button::show()
{
	int t1,t2,k=0,i;
	setlinestyle(0,1,1);
	for(i=0;i<strlen(Button_Name);i++)
	k=k+4;
	t1=(X2-X1)/2+X1-k;
	t2=(Y2-Y1)/2+Y1;
	Unpushed_Button(X1,Y1,X2,Y2,Button_Color);
	settextstyle(0,0,0);
	setcolor(Text_Color);
	outtextxy(t1,t2,Button_Name);
}
IconButton::IconButton(int x1,int y1,int c,char icfn[12])
{
	strcpy(iconfn,icfn);
	Button_Color=c;
	Text_Color=0;
	X1=x1;
	Y1=y1;
	Image icon(iconfn);
	icon.getSize(iconwidth,iconheight);
	X2=X1+iconwidth+5;
	Y2=y1+iconheight+5;
	boarder=true;
}
int IconButton::toggle()
{
	get_pointer_position(&button,&x,&y);
	if( (x>X1 && x<X2) && (y>Y1 && y<Y2) && (button==1))
	{
			pointer(HIDE);
			Pushed_Button(X1,Y1,X2,Y2,Button_Color);
			Image icon(iconfn);
			icon.putImage(X1+3,Y1+3);
			pointer(SHOW);
			while((button==1)&&(x>X1 && x<X2) && (y>Y1 && y<Y2) && (button==1))
			get_pointer_position(&button,&x,&y);
			pointer(HIDE);
			{
				Image icon(iconfn);
				Unpushed_Button(X1,Y1,X2,Y2,Button_Color);
				icon.putImage(X1+2,Y1+2);
			}
			pointer(SHOW);
			if( (x>X1 && x<X2) && (y>Y1 && y<Y2))
			return ClicKed;
			else
			return Not_ClicKed;
	}
		else
		return Not_ClicKed;

}
void IconButton::show()
{
	Unpushed_Button(X1,Y1,X2,Y2,Button_Color);
	Image icon(iconfn);
	icon.putImage(X1+2,Y1+2);
}
void Window::sLine(int x,int y,int x1,int y1)
{
	setlinestyle(0,1,1);
	setcolor(BLACK);
	line(x,y,x1,y1);
	setcolor(WHITE);
	line(x+1,y+1,x1-1,y1+1);
}
void Window::setOrigin(int x,int y)
{X1=x;Y1=y;originsetted=true;}
void Window::setSize(int width,int height)
{
	int cwidth=width/2;
	int cheight=height/2;
	if(!originsetted)
	{
		X1=getmaxx()/2-cwidth;
		X2=getmaxx()/2+cwidth;
		Y1=getmaxy()/2-cheight;
		Y2=getmaxy()/2+cheight;
	}
	if(originsetted)
	{X2=X1+width;Y2=Y1+height;}
}
void Window::setTitle(char title[60])
{strcpy(Window_Name,title);}
void Window::setBarColor(int color)
{bar_color=color;}
void Window::setColor(int color)
{Window_Color=color;}
void Window::setTitleColor(int color)
{textcolor=color;}
void Window::setDate(boolean date)
{write_date=date;}
void Window::setBar(boolean bar)
{showbar=bar;}
void Window::show()
{
		 setlinestyle(0,1,1);
		 setfillstyle(SOLID_FILL,Window_Color);
		 bar(X1,Y1,X2,Y2);
		 setcolor(WHITE);
		 if(Window_Color!=bar_color){
		 line(X1,Y1,X2,Y1);
		 line(X1,Y1,X1,Y2);
		 setcolor(BLACK);
		 line(X1,Y2,X2,Y2);
		 line(X2,Y1,X2,Y2);}
		 if(Window_Color==bar_color){
		 setcolor(BLACK);
		 line(X1,Y1,X2,Y1);
		 line(X1,Y1,X1,Y2);
		 setcolor(WHITE);
		 line(X2,Y1,X2,Y2);
		 line(X2,Y2,X1,Y2);}
		 if(Window_Color==BLUE&&showbar)
		 {setfillstyle(SOLID_FILL,RED);}
		 if(Window_Color!=BLUE&&showbar){setfillstyle(SOLID_FILL,bar_color);}
		 bar(X1+1,Y1+1,X2-1,Y1+barheight);
		 if((!strcmp(Window_Name,"")));
		 {setcolor(textcolor);
		 outtextxy(X1+3,Y1+10,Window_Name);}
		 if(write_date)
		 WRITE_DATE(X2-80,Y1-200);
}
void Window::WRITE_DATE(int X2, int Y1)
{
	int a,b,c;
	char *month;
	getdate(&d);
	char d1[10],d2[10],d3[10];
	setcolor(YELLOW);
	a=d.da_year;
	b=d.da_day;
	c=d.da_mon;
	switch(c)
		{
			case 1:	outtextxy(X2-60,Y1+210,"January");break;
			case 2:	outtextxy(X2-64,Y1+210,"Feburary");break;
			case 3:	outtextxy(X2-64,Y1+210,"March");break;
			case 4:	outtextxy(X2-64,Y1+210,"April");break;
			case 5:	outtextxy(X2-30,Y1+210,"May");break;
			case 6: outtextxy(X2-30,Y1+210,"June");break;
			case 7: outtextxy(X2-30,Y1+210,"July");break;
			case 8: outtextxy(X2-45,Y1+210,"August");break;
			case 9: outtextxy(X2-64,Y1+210,"September");break;
			case 10: outtextxy(X2-56,Y1+210,"October");break;
			case 11: outtextxy(X2-64,Y1+210,"November");break;
			case 12: outtextxy(X2-64,Y1+210,"December");break;
		  }
	itoa(a,d1, 10);
	itoa(b,d2, 10);
	itoa(c,d3, 10);
	outtextxy(X2+5,Y1+210,d2);
	outtextxy(X2+33,Y1+210,d1);
	setcolor(BLACK);
}
Menu::Menu(int x,int y,char mn[])
{
	strcpy(menuname,mn);
	itemindex=0;
	highlightheight=12;
	height=0;
	width=0;
	X1=x;
	Y1=y;
	Y1+=10;
	tx=x;
	ty=y;
	ewidth=0;
	menucolor=7;
	textcolor=0;
	hlcolor=1;
	hltextcolor=15;
	disableitemcolor=8;
	selectedindex=-1;
	for(int i=0;i<30;i++)
	separator[i]=0;
	for( i=0;i<30;i++)
	disable[i]=0;
	boarder=true;
	type=MENU;
}
void Menu::show()
{
	settextstyle(0,0,1);
	setcolor(BLACK);
	outtextxy(X1,ty,menuname);
}
boolean Menu::toggle()
{
	nw=textwidth(menuname);
	nh=textheight(menuname);
	settextstyle(0,0,1);
	get_pointer_position(&button,&mx,&my);
	if(mx>X1&&mx<X1+nw&&my>ty&my<ty+nh)
	{
		pointer(HIDE);
		setfillstyle(1,1);
		bar(X1-3,ty-3,X1+nw+2,ty+nh+3);
		setcolor(15);
		outtextxy(X1,ty,menuname);
		pointer(SHOW);
		while(mx>X1&&mx<X1+nw&&my>ty&my<ty+nh)
		{
				get_pointer_position(&button,&mx,&my);
				if(button==1)
				{
					pointer(SHOW);
					return true;
				 }
		  }
		  pointer(HIDE);
		  setfillstyle(1,7);
		  bar(X1-3,ty-3,X1+nw+2,ty+nh+3);
		  setcolor(textcolor);
		  outtextxy(X1,ty,menuname);
		  pointer(SHOW);
	}
	return false;
}
void Menu::setExtraWidth(int ew)
{ewidth=ew;}
void Menu::setColor(int color)
{menucolor=color;}
void Menu::setHighlightColor(int color)
{hlcolor=color;}
void Menu::setTextColor(int color)
{textcolor=color;}
void Menu::setHighlightTextColor(int color)
{hltextcolor=color;}
void Menu::setDisableTextColor(int color)
{disableitemcolor=color;}
void Menu::findSize()
{
	int i=0;
	int max=0,maxchar;
	height=0;width=0;
	while(i<itemindex)
	{
		height+=20;
		maxchar=strlen(menuitem[i]);
		if(maxchar>max)
			{
			   max=maxchar;
			   width=textwidth(menuitem[i])+10;
			}
		i++;
	}
	  X2=X1+width;
	  Y2=Y1+height;
 }
void Menu::add(char item[])
{
	strcpy(menuitem[itemindex],item);
	itemindex++;
	menuitem[itemindex][0]='\0';
}
void Menu::add(int index,char item[])
{
	int i;
	if(index<itemindex+1)
	{
		for(i=itemindex-1;i>=index-1;i--)
		strcpy(menuitem[i+1],menuitem[i]);
		strcpy(menuitem[index-1],item);
		for(i=itemindex-1;i>=index-1;i--)
		separator[i+1]=separator[i];
		for(i=itemindex-1;i>=index-1;i--)
		disable[i+1]=disable[i];
		itemindex++;
		return;
	}
	else
	return;
}
void Menu::remove(int index)
{
	for(int i=index;i<itemindex;i++)
	strcpy(menuitem[i-1],menuitem[i]);
	for(i=index;i<itemindex;i++)
	separator[i-1]=separator[i];
	for(i=index;i<itemindex;i++)
	disable[i-1]=disable[i];
	itemindex--;
}
void Menu::remove(char item[])
{
	int removeindex;
	for(int i=0;i<itemindex;i++)
	{
		if((strcmp(menuitem[i],item))==0)
			{
				removeindex=i+1;
				break;
			}
	}
	remove(removeindex);
}
void Menu::setEnable(int index,boolean flag)
{
	if(flag)
	disable[index-1]=0;
	else
	disable[index-1]=1;
}
void Menu::setEnable(char item[],boolean flag)
{
	int index;
	for(int i=0;i<itemindex;i++)
	{
		if((strcmp(menuitem[i],item))==0)
			{
				index=i+1;
				break;
			}
	}
	setEnable(index,flag);
}
void Menu::setBoarder(boolean flag)
{
	boarder=flag;
}
void Menu::addSeparator()
{
	separator[itemindex]=1;
}
void Menu::drawSeparator()
{
	setcolor(BLACK);
	line(X1+1,Y1+l-1,X2-2,Y1+l-1);
	setcolor(WHITE);
	line(X1+2,Y1+l,X2-3,Y1+l);
}
void Menu::drawMenu()
{
	int i=0;
	l=0;
	pointer(HIDE);
	settextstyle(0,0,1);
	findSize();
	Image  menuimg("Menuimg.ks");
	menuimg.getImage(X1,Y1,X2+ewidth,Y2);
	setfillstyle(1,menucolor);
	bar(X1,Y1,X2+ewidth,Y2);
	if(boarder)
	{
		setcolor(WHITE);
		line(X1,Y1,X2+ewidth,Y1);
		line(X1,Y1,X1,Y2);
		setcolor(BLACK);
		line(X2+ewidth,Y1,X2+ewidth,Y2);
		line(X1,Y2,X2+ewidth,Y2);
	}
	while(i<itemindex)
	 {
		if(disable[i]==1)
		setcolor(disableitemcolor);
		else
		setcolor(textcolor);
		outtextxy(X1+5,Y1+5+l,menuitem[i]);
		if(separator[i]==1)
		drawSeparator();
		i++;
		l+=20;
	 }
  pointer(SHOW);
}
int Menu::getSelectedIndex()
{return selectedindex;}
char *Menu::getSelectedItem()
{
	int length=strlen(menuitem[selectedindex-1]);
	selecteditem=new char[length];
	strcpy(selecteditem,menuitem[selectedindex-1]);
	return selecteditem;
}
void Menu::handleMenu(const int x,const int y)
{
	this->X1=x;
	this->Y1=y;
	this->handleMenu();
}

void Menu::HighlightItem()
{
   highlightedposition=currenty1-Y1;//determine the mouse position on the menu
   itmindx=highlightedposition/20;//determine the index value of the menu item,each item height diffrence is 20 pixels
   if(disable[itmindx])//if the current item is disabled
   return;
   pointer(HIDE);
   setfillstyle(1,hlcolor);
   bar(X1+2,currenty1-3,X2+ewidth-2,currenty1+highlightheight);//highlight the current item
   setcolor(hltextcolor);
   outtextxy(X1+5,currenty1,menuitem[itmindx]); //print the item name from the array
   pointer(SHOW);
}
void Menu::deHighlightItem()
{
   highlightedposition=currenty1-Y1;//determine the mouse position on the menu
   itmindx=highlightedposition/20;//determine the index value of the menu item,each item height diffrence is 20 pixels
   if(disable[itmindx])
   return;
   pointer(HIDE);
   setfillstyle(1,menucolor); //reset the highlighted item  when mouse pointer goes out of the current item
   bar(X1+2,currenty1-3,X2+ewidth-2,currenty1+highlightheight);
   setcolor(textcolor);
   outtextxy(X1+5,currenty1,menuitem[itmindx]);
   pointer(SHOW);
}
void Menu::handleMenu()
{
  itmindx=-1;
  currenty1=Y1+5;//item will be highlighted from the menu Y1+5 position
  int exitcode=1,count=1,tcy,check=0;
  char ch;
  int key;
  drawMenu();
  settextstyle(0,0,1);
  if(type==CHOICE)
  {
	for(int i=1;i<getSelectedIndex();i++)
	currenty1+=20;
	pointer(HIDE);
	setfillstyle(1,hlcolor);
	bar(X1+2,currenty1-3,X2+ewidth-2,currenty1+highlightheight);//highlight the current item
	setcolor(hltextcolor);
	outtextxy(X1+5,currenty1,menuitem[getSelectedIndex()-1]); //print the item name from the array
	pointer(SHOW);
  }
  while(exitcode)
  {
	get_pointer_position(&button,&mx,&my);
	  if(kbhit())
	  {
			ch=getch();
			if(ch==27)
			 {
				dispose(-1);
				return;
			 }
			if(ch==13&&!disable[itmindx])
			{
			   dispose(itmindx+1);
			   return;
			}
			if(ch==0)
			{
				key=getch();
				if(key==80)
				{
					deHighlightItem();
					if(currenty1>=Y2-20)
					currenty1=Y1+5;
					else
					currenty1+=20;

					if(!check&&type==MENU)
					currenty1=Y1+5;
					HighlightItem();
					check=1;
				}
			   if(key==72)
			   {
					deHighlightItem();
					if(currenty1<=Y1+5)
					currenty1=Y2-15;
					else
					currenty1-=20;
					HighlightItem();
					check=1;
			   }
			  tcy=currenty1;

		  }
	}

	if((mx>X1&&mx<X1+width)&&(my>Y1-10&&my<Y1+height))//if the mouse pointer is with in the menu
			{
				 if(type==CHOICE&&count)
				   {
						pointer(HIDE);
						setfillstyle(1,menucolor); //reset the highlighted item  when mouse pointer goes out of the current item
						bar(X1+2,currenty1-3,X2+ewidth-2,currenty1+highlightheight);
						setcolor(textcolor);
						outtextxy(X1+5,currenty1,menuitem[getSelectedIndex()-1]);
						pointer(SHOW);
						count=0;
					}
				if(mx>X1&&mx<X1+width+ewidth&&my>currenty1&&my<currenty1+highlightheight)
				//here currenty1 will be changing between the Y1 postion and Y2 position of the menu
						{
							if(check)
							{
							   highlightedposition=tcy-Y1;//determine the mouse position on the menu
							   itmindx=highlightedposition/20;//determine the index value of the menu item,each item height diffrence is 20 pixels
							   if(disable[itmindx])
							   continue;
							   pointer(HIDE);
							   setfillstyle(1,menucolor); //reset the highlighted item  when mouse pointer goes out of the current item
							   bar(X1+2,tcy-3,X2+ewidth-2,tcy+highlightheight);
							   setcolor(textcolor);
							   outtextxy(X1+5,tcy,menuitem[itmindx]);
							   pointer(SHOW);
							}
							check=0;
							HighlightItem();
							while(mx>X1&&mx<X1+width&&my>currenty1&&my<currenty1+highlightheight)//highlight until mouse pointer is in the current item
							{
								get_pointer_position(&button,&mx,&my);
								if(button==1&&!disable[itmindx])//if mouse is clicked on the item
									{
									  dispose(itmindx+1);
									  return;
									 }

							}
							deHighlightItem();

						}
						currenty1+=20;  //increase currenty1 by 20
						if(currenty1>Y2) //if currenty1 goes out of menu size reset currenty1
						currenty1=Y1+5;


				}
			  if(button==1&&(mx>X2||mx<X1||my>Y2||my<Y1)||button==2&&(mx>X2||mx<X1||my>Y2||my<Y1))//if clicked outside the menu
				{
					dispose(-1);
					return;
				}
    }//end of while
}
void Menu::dispose(int selectedindex)
{
	Image  menuimg("Menuimg.ks");
	menuimg.putImage();    //clear the menu
	menuimg.clearFile();
	if(type==MENU)
		{
			pointer(HIDE);
			setfillstyle(1,7);
			bar(X1-3,ty-3,X1+nw+2,ty+nh+3);
			setcolor(BLACK);
			outtextxy(X1,ty,menuname);
			pointer(SHOW);
		 }
	this->selectedindex=selectedindex;
}
Choice::Choice(int x,int y)
{
	X1=x;
	Y1=y;
	tx=x+4;
	ty=y+4;
	itemindex=0;
	highlightheight=12;
	height=0;
	width=0;
	menucolor=15;
	textcolor=0;
	hlcolor=1;
	hltextcolor=15;
	disableitemcolor=8;
	selectedindex=1;
	boarder=true;
	cx=X1;
	cy=Y1+16;
	for(int i=0;i<30;i++)
	separator[i]=0;
	for( i=0;i<30;i++)
	disable[i]=0;
	type=CHOICE;
}
void Choice::setSelectedIndex(int index)
{
	if(index<=itemindex)
	selectedindex=index;
	else
	selectedindex=1;
}
void Choice::setSelectedItem(char item[])
{
	int index;
	for(int i=0;i<itemindex;i++)
	{
		if((strcmp(menuitem[i],item))==0)
		   {
				index=i+1;
				break;
			}
	}
	setSelectedIndex(index);
}
void Choice::show()
{
	findSize();
	setfillstyle(1,menucolor);
	bar(X1,Y1,X2+13,Y1+17);
	setcolor(BLACK);
	line(X1,Y1,X2+13,Y1);
	line(X1,Y1,X1,Y1+17);
	setcolor(WHITE);
	line(X2+13,Y1,X2+13,Y1+17);
	line(X1,Y1+17,X2+13,Y1+17);
	setcolor(BLACK);
	line(X1+1,Y1+16,X2+13,Y1+16);
	setcolor(WHITE);
	line(X1+2,Y1+15,X2+13,Y1+15);
	setfillstyle(1,hlcolor);
	bar(X1+2,Y1+2,X2-2,Y1+15);
	setcolor(hltextcolor);
	outtextxy(tx,ty,menuitem[selectedindex-1]);
	choicebutton=new Button(X2,Y1+1,"");
	choicebutton->setHeight(14);
	choicebutton->setWidth(12);
	choicebutton->show();
}
boolean Choice::toggle()
{
	if(choicebutton->toggle())
	return true;
	return false;
}
void Choice::handle()
{
	prevselected=getSelectedIndex();
	setExtraWidth(14);
	handleMenu(cx,cy+2);
	selectedindex=getSelectedIndex();
	if(selectedindex==-1)
	selectedindex=prevselected;
	setExtraWidth(0);
	setfillstyle(1,menucolor);
	bar(X1+2,Y1-16,X2-1,Y1-5);
	setfillstyle(1,hlcolor);
	bar(X1+2,Y1-16,X2-2,Y1-5);
	setcolor(hltextcolor);
	outtextxy(tx,ty,menuitem[selectedindex-1]);
 }
void Frame::setSize(int width,int height)
{
	int cwidth=width/2;
	int cheight=height/2;
	if(!originsetted)
	{
		X1=getmaxx()/2-cwidth;
		X2=getmaxx()/2+cwidth;
		Y1=getmaxy()/2-cheight;
		Y2=getmaxy()/2+cheight;
	}
	if(originsetted)
	{X2=X1+width;Y2=Y1+height;}
	winclosebutton=new Button(X2-18,Y1+4,"X");
	winclosebutton->setWidth(16);
	winclosebutton->setHeight(15);
}
void Frame::show()
{
		 Image winimg(winflnam);
		 winimg.getImage(X1,Y1,X2,Y2);
		 m.pointer(HIDE);
		 setlinestyle(0,1,1);
		 setfillstyle(SOLID_FILL,Window_Color);
		 bar(X1,Y1,X2,Y2);
		 setcolor(WHITE);
		 if(Window_Color!=bar_color){
		 line(X1,Y1,X2,Y1);
		 line(X1,Y1,X1,Y2);
		 setcolor(BLACK);
		 line(X1,Y2,X2,Y2);
		 line(X2,Y1,X2,Y2);}
		 if(Window_Color==bar_color){
		 setcolor(BLACK);
		 line(X1,Y1,X2,Y1);
		 line(X1,Y1,X1,Y2);
		 setcolor(WHITE);
		 line(X2,Y1,X2,Y2);
		 line(X2,Y2,X1,Y2);}
		 if(Window_Color==BLUE&&showbar)
		 {setfillstyle(SOLID_FILL,RED);}
		 if(Window_Color!=BLUE&&showbar){setfillstyle(SOLID_FILL,bar_color);}
		 bar(X1+1,Y1+1,X2-1,Y1+20);
		 if((!strcmp(Window_Name,"")));
		 {setcolor(textcolor);
		 outtextxy(X1+3,Y1+10,Window_Name);}
		 winclosebutton->show();
		 if(write_date)
		 WRITE_DATE(X2-80,Y1-200);
		 m.pointer(SHOW);
}
boolean Frame::windowClosed()
{
	if(winclosebutton->toggle())
	return true;
	return false;
}
void Frame::closeFrame()
{Image winimg(winflnam);winimg.putImage();winimg.clearFile();}
TextField::TextField()
{

				WNO=10;
				str=new char[WNO+1];
				HEIGHT=20;
				fieldcolor=17;
				textcolor=0;
				cursorcolor=0;
				cursorheight=17;
				xPos=10;
				yPos=10;
				width=WNO*9;
				escapekey=notpressed;
				textpresent=false;
				editable=true;
				c=0;
				w=1;
}
void TextField::initialiseTextField(int x,int y,int wordno)
{

				WNO=wordno;
				str=new char[WNO+1];
				HEIGHT=17;
				fieldcolor=15;
				textcolor=0;
				cursorcolor=0;
				cursorheight=17;
				xPos=x;
				yPos=y;
				len=new char[WNO];
				for(int i=0;i<=WNO;i++)
				len[i]='t';
				len[--i]='\0';
				width=textwidth(len)+4;
				delete len;
				escapekey=notpressed;
				textpresent=false;
				border=true;
				editable=true;
				c=0;
				w=1;
}
void TextField::setTextColor(int tcolor)
{textcolor=tcolor;}
void TextField::setFieldColor(int fcolor)
{fieldcolor=fcolor;}
void TextField::setCursorColor(int ccolor)
{cursorcolor=ccolor;}
int TextField::getTextColor()
{return textcolor;}
int TextField::getFieldColor()
{return fieldcolor;}
int TextField::getCursorColor()
{return cursorcolor;}
void TextField::showField()
{
	if(editable)
	setfillstyle(1,fieldcolor);
	if(!editable)
	setfillstyle(1,7);
	bar(xPos,yPos,xPos+width,yPos+HEIGHT);
	if(border)
	{
		setcolor(0);
		line(xPos,yPos,xPos+width,yPos);
		line(xPos,yPos,xPos,yPos+HEIGHT);
		setcolor(15);
		line(xPos,yPos+HEIGHT,xPos+width,yPos+HEIGHT);
		line(xPos+width,yPos,xPos+width,yPos+HEIGHT);
	}
}
void TextField::show()
{
	showField();
	if(textpresent)
	printText();
}
void TextField::printText()
{setcolor(textcolor);outtextxy(xPos+4,yPos+5,str);}
void TextField::setEditable(boolean editb)
{
	editable=editb;
	show();
}
void TextField::setBorder(boolean border)
{
	this->border=border;
}
boolean TextField::isEditable()
{
	if(editable)
	return true;
	else
	return false;
}
boolean TextField::toggle()
{
	Mousehandler.get_pointer_position(&b,&x,&y);
	if(editable)
	if(b&1==1&&x>=xPos&&y>=yPos&&x<=xPos+width&&y<=yPos+HEIGHT)
	return true;
	return false;
}
void TextField::setText(char text[])
{
	showField();
	setcolor(textcolor);
	settextstyle(DEFAULT_FONT,HORIZ_DIR,1);
	textpresent=true;
	strcpy(str,text);
	c=strlen(text);
	printText();
	return;
}
char *TextField::getText()
{
	if(textpresent)
	return str;
	else
	return NULL;
}
char *TextField::handle()
{
	int t,max=0;
	setlinestyle(0,0,0);
	showField();
	settextstyle(DEFAULT_FONT,HORIZ_DIR,1);
	Mousehandler.pointer(HIDE);
	if(textpresent==true)
	{
		w=c+1;
		c=w-1;
		printText();
	}
	else
	{
		w=1;
		c=0;
		strcpy(str,"");
		showField();
	}
			 max=c;
			 int key=0;
				do
					 {
						while(!kbhit())
						{
							setcolor(cursorcolor);
							line(xPos+w*8-5,yPos+3,xPos+w*8-5,yPos+cursorheight-3);
							delay(100);
							setcolor(fieldcolor);
							line(xPos+w*8-5,yPos+3,xPos+w*8-5,yPos+cursorheight-3);
							delay(120);
						}
						ch=getch();
						setcolor(fieldcolor);
						if(ch==8&&c>0)
						{
						  max--;c--;w--;
						  for(int i=c;i<max;i++)
						  str[i]=str[i+1];
						  str[max]='\0';
						  showField();
						}
						if(c>max)
						max=c;
						int tabkey=(int)ch;
						if(tabkey==9)
						{
							if(c!=0)
							textpresent=true;
							return str;
						}

						if(ch==0&&c>=0)
						  {
								key=getch();
								if(key==75&&c!=0)//left arrow pressed
									{
										w--;
										c--;
										str[max]='\0';
									}
								if(key==77&&w<=max)//right arrow pressed
									{
										w++;
										c++;
										if(c>max)
										max=c;
										str[max]='\0';
									}
								if(key==83&&w<=max)//delete key
									{
										for(int i=c;i<=max;i++)
										str[i]=str[i+1];//delete the character from the arrayindex,c
										max--;
										showField();
									}
								 if(key==71)//home key
								 {
									w=1;
									c=0;
								 }
								 if(key==79)//end key
								 {
									w=max+1;
									c=max;
								  }
							  }
						if(c<0)
						c=0;
						if(w==0)
						w=1;
						if(ch==27)// esacape key
						{escapekey=pressed;textpresent=false;showField();Mousehandler.pointer(SHOW);return NULL;}
					if(ch!='\r'&&ch!=8&&c<WNO&&key!=75&&key!=77&&key!=83&&key==0)
						  {
							 if(c<=max&&max<=WNO-1)
							 {
							   showField();
							   for(int i=max;i>=c;i--)
							   str[i+1]=str[i];
							   str[c++]=ch;
							   max++;
							 }
							else
							str[c++]=ch;
							if(c<=max)
							showField();
							if(c>max)
							max=c;
							str[max]='\0';
							w++;
						 }
				printText();
				key=0;
			}while(ch!='\r');
  Mousehandler.pointer(SHOW);
  if(c!=0)
  textpresent=true;
  return str;
}

void PasswordField::initialisePasswordField(int x,int y,int WNO)
{
	initialiseTextField(x,y,WNO);
	password=new char[WNO+1];
	passchar='*';
}
void PasswordField::setCharacter(char ch)
{passchar=ch;}
void PasswordField::show()
{
	showField();
	if(textpresent)
	printPassword();
}
void PasswordField::setEditable(boolean flag)
{
	editable=flag;
	show();
}
void PasswordField::setText(char text[])
{
	settextstyle(DEFAULT_FONT,HORIZ_DIR,1);
	showField();
	setcolor(textcolor);
	settextstyle(DEFAULT_FONT,HORIZ_DIR,1);
	textpresent=true;
	strcpy(str,text);
	c=strlen(text);
	for(int i=0;i<c;i++)
	password[i]=passchar;
	outtextxy(xPos+4,yPos+5,password);
	return;
}
void PasswordField::printPassword()
{setcolor(textcolor);outtextxy(xPos+4,yPos+5,password);}
char *PasswordField::handle()
{
	int t,max=0;
	showField();
	settextstyle(DEFAULT_FONT,HORIZ_DIR,1);
	Mousehandler.pointer(HIDE);
	if(textpresent==true)
	{
		w=c+1;
		c=w-1;
		printPassword();
	}
	else
	{
		w=1;
		c=0;
		showField();
		strcpy(str,"");
	}
	   max=c;
	   int key=0;
		settextstyle(0,0,1);
		do
			 {
						while(!kbhit())
						{
							setcolor(cursorcolor);
							line(xPos+w*8-5,yPos+3,xPos+w*8-5,yPos+HEIGHT-3);
							delay(100);
							setcolor(fieldcolor);
							line(xPos+w*8-5,yPos+3,xPos+w*8-5,yPos+HEIGHT-3);
							delay(120);
						}
						ch=getch();
						if(ch==8&&c>0)
						{
						  max--;c--;w--;
						  for(int i=c;i<max;i++)
						  str[i]=str[i+1];
						  str[max]='\0';
						  showField();
						}
						if(c>max)
						max=c;
						if(ch==0&&c>=0)
						  {
								key=getch();
								if(key==75&&c!=0)//left arrow pressed
									{
										w--;  //decrease the cursor position
										c--;//decreease the array index
										str[max]='\0';
									}
								if(key==77&&w<=max)//right arrow pressed
									{
										w++;
										c++;
										if(c>max)
										max=c;
										str[max]='\0';
									}
								if(key==83&&w<=max)//delete key pressed
									{
										for(int i=c;i<=max;i++)
										str[i]=str[i+1];
										max--;
										showField();
									}
								 if(key==71)//home key
								 {
									w=1;
									c=0;
								 }
								 if(key==79)  //end key
								 {
									w=max+1;
									c=max;
								  }

							  }
						if(c<0)
						c=0;
						if(w<0)
						w=1;
						if(ch==27)
						{escapekey=pressed;textpresent=false;showField();Mousehandler.pointer(SHOW);return NULL;}
						if(ch!='\r'&&ch!=8&&c<WNO&&key!=75&&key!=77&&key!=83&&key==0)
						   {
							 if(c<=max&&max<=WNO-1)
							 {
							   showField();
							   for(int i=max;i>=c;i--)
							   str[i+1]=str[i];
							   str[c++]=ch;
							   max++;
							 }
							else
							str[c++]=ch;
							if(c<=max)
							showField();
							if(c>max)
							max=c;
							str[max]='\0';
							w++;
						   }
						   for(int i=0;i<=max;i++)
						   password[i]=passchar;
						   password[max]='\0';
					printPassword();
				key=0;
			}while(ch!='\r');
  w=max;
  c=max;
  Mousehandler.pointer(SHOW);
  textpresent=true;
  return str;
}
void MessageBox::setColor(int color)
{fillcolor=color;return;}
void MessageBox::setTextColor(int color)
{textcolor=color;}
void MessageBox::setBarColor(int color)
{barcolor=color;return;}
void MessageBox::showMessageBox(char *title,char *msg)
{
	strcpy(message_title,title);
	strcpy(message,msg);
	showMessageBox();
}
void MessageBox::showMessageBox()
{
	   int t1,t2,k=0,i;
	   Image bximg("msgbimg.ks");
	   messglength=strlen(message);
	   boolean nextline=false;
	   KeyboardListener ml;
	   i=0;
	   int l=0;
	   int c=0;
	   int width=0,height=80;//default height of the message window
	   int maxchar=0;
	   int max;
	   int temp=0;
	   char *msg1=new char [300];//maximum one one line message array
	   char *msg2=new char [300];
//determine width & height of the MessageBox
	   while(c<messglength)
		{
			msg1[i]=message[c];//copy the message
			i++;
			c++;
			if(msg1[i-2]=='/'&&msg1[i-1]=='n')//if /n found in the copied array
			 {
					  nextline=true;
					  msg1[i-2]='\0'; //do not include the '/n'
					  max=strlen(msg1)+3;
					  if(max>maxchar)//find the greater one
					  {
							maxchar=max;
							width=textwidth(msg1);//set the width of window
					  }
					  temp=i;//hold the value of i (index of the array)
							 //to copy the charcters into another array
					  height+=12;//increase height by 12 pixels per line
			 }
			if(temp!=0)
				{
					int t=0;
					for(int k=temp;k<=i;k++,t++)
					msg2[t]=msg1[k];//copy the array from the last /n found
									//previous positon
					msg2[t]='\0';
					max=strlen(msg2)+3;
					if(max>maxchar)       //check the greater string length
					 {
						maxchar=max;
						width=textwidth(msg2);//set the width of the window
					 }
				}
	  } //end of while, width & height determined here
	   if(!nextline)//if only a single line is in the message
	   width=textwidth(message)+10;
	   if(width<120)//if width is too small set the default width
	   width=140;
	   i=0;c=0;
	   int cwidth=width/2;
	   X1=getmaxx()/2-cwidth;
	   X2=getmaxx()/2+cwidth;
	   int cheight=height/2;
	   Y1=getmaxy()/2-cheight;
	   Y2=getmaxy()/2+cheight;
	   buttxpos=((X2-X1)/2+X1)-37;
	   setlinestyle(0,1,1);
	   Window mess(X1,Y1,X2,Y2,fillcolor,message_title,barcolor,false);
	   Button closebutt(X2-20,Y1+3,X2-3,Y1+18,7,0,"X");
	   Button okbutt(buttxpos,Y2-30,"OK");
		okbutt.setWidth(70);
	   m.pointer(HIDE);
	   bximg.getImage(X1,Y1,X2,Y2);
	   ml.add(okbutt);
	   mess.show();
	   closebutt.show();
	   okbutt.show();
	   char *msg=new char [maxchar];
	   setcolor(textcolor);
	   settextstyle(DEFAULT_FONT,HORIZ_DIR,1);
	   while(c<messglength)
		   {
				msg[i]=message[c];
				i++;
				c++;
				if(i==maxchar||msg[i-2]=='/'&&msg[i-1]=='n')//checks for next line or maximum one line message
				{
				   if(msg[i-2]=='/'&&msg[i-1]=='n')
				   msg[i-2]='\0';   //if /n found do not include /n in array
				   else
				   msg[i]='\0';
				   for(int j=0;j<strlen(msg);j++)
				   k=k+4;
				   t1=(X2-X1)/2+X1-k;//determine the center position for the text in the window
				   t2=((Y2-Y1)/2+Y1)-(Y2-Y1)/4;
				   outtextxy(t1,t2+l,msg);//print the message array
				   i=0;
				   k=0;
				   l=l+10;
				}
			}
			k=0;
			msg[i]='\0';
			for(int j=0;j<strlen(msg);j++)
			k=k+4;
			t1=(X2-X1)/2+X1-k;
			t2=((Y2-Y1)/2+Y1)-(Y2-Y1)/4;
			if(!nextline)//if single line
			outtextxy(t1,t2+10,msg);
			else
			outtextxy(t1,t2+l,msg);//print the last line of message
	   m.pointer(SHOW);
	   while(1)
	   {
		 ml.enableKeyboardAccess();
		if(closebutt.toggle())
		 {
			bximg.putImage();
			bximg.clearFile();
			return;
		 }
		if(okbutt.toggle())
		 {
			bximg.putImage();
			bximg.clearFile();
			return;
		 }


	   }
}
int Mouse::initialise_mouse()
{
	input.x.ax=0;
	int86(0x33,&input,&output);
	return(input.x.ax);
}
int Mouse::pointer(int ON)
{
	input.x.ax=ON;
	int86(0x33,&input,&output);
	return 0;
}
int Mouse::restrict_pointer(int X1,int Y1,int X2,int Y2)
{
	input.x.ax=7;
	input.x.cx=X1;
	input.x.dx=X2;
	int86(0x33,&input,&output);
	input.x.ax=8;
	input.x.cx=Y1;
	input.x.dx=Y2;
	int86(0x33,&input,&output);
	return(0);
}
int Mouse::change_pointer(int X1,int Y1)
{
	input.x.ax=4;
	int86(0x33,&input,&output);
	input.x.cx=X1;
	input.x.dx=Y1;
	return(0);
}
int Mouse::get_pointer_position(int *Button,int *X,int *Y)
{
	 input.x.ax=3;
	 int86(0x33,&input,&output);
	 *Button=output.x.bx;
	 *X=output.x.cx;
	 *Y=output.x.dx;
	 return 0;
}
class IMAGE//saves image of area 81 pixels width and 176 pixels height in  an array
{
	private:
			int IMAGES[81][176];
			int x1pix,y1pix;
			int  maxrow,maxcol;

	public:
			IMAGE(int x1p,int y1p)
				{
					x1pix=x1p;
					y1pix=y1p;
					maxrow=176;
					maxcol=81;
				}
			 void GetImage();
			 void PutImage();
};

void IMAGE::GetImage()
{
   for(int i=0;i<maxcol;i++)
   for (int j=0;j<maxrow;j++)
   IMAGES[i][j]=getpixel(x1pix+j,y1pix+i);
   IMAGES[++i][++j]='/0';
   return;

}
void IMAGE::PutImage()
{
   for(int i=0;i<maxcol;i++)
	 {
		for (int j=0;j<maxrow;j++)
			{
				putpixel(x1pix+j,y1pix+i,IMAGES[i][j]);
			}
	 }
   return;
}